import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class APITests {

    @Test
    public void test(){
        String token="BQBRZA8NSB1E1biuPMXve3Ll5dbt5UWbJrExvvdaMDea3-p5rISXSn1NAwNaERKiAY8cx66k0UQ8761IEnvQiag9eu20fCe-0N7mUoSqybHre0Q5WxYTIhKOLaanrpt7_5RZCK40HNFw_IoP";
        String url="https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?market=ES&limit=50&offset=5";
        //Response response= RestAssured.get("https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?market=ES&limit=50&offset=5");
        //System.out.println("Response : "+ response.asString());
        Response response =
                given()
                        .headers(
                                "Authorization",
                                "Bearer " + token
                               )
                        .when()
                        .get(url)
                        .then()
                        .contentType(ContentType.JSON)
                        .extract()
                        .response();
        System.out.println(response.getBody().prettyPrint());


    }
}
