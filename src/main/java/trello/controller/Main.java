package trello.controller;

import org.json.JSONArray;
import trello.data.DataSpotifyAPI;
import trello.domain.Album;
import trello.domain.TrelloBoard;
import trello.domain.TrelloList;
import trello.service.InputDataBuilder;
import trello.service.ReadData;
import trello.service.TrelloDataPost;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ReadData rd = new ReadData();
        InputDataBuilder idb=new InputDataBuilder();
        TrelloDataPost tdp=new TrelloDataPost();
        File discographyFile = new File("src/main/java/trello/assets/discography.txt");
        String spotifyToken = "BQA4k3ALmd-J5mG-su-xE2MDBX31GPOQ2cDBQ7JgE4cokz3i7ibC6SCJlfnw7CoteTm0D_bgsM8jKqhLwWqoGYniQ5Vd3BtqzSSBm5cCO0FYoDpHLwLxcIbzvdXeh1bYoHXLit0deTZxAeGj5_nz-XSNgeiWE9sREJs";
        String spotifyUrl = "https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?market=ES&limit=50&offset=5";
        String trelloUrl="https://api.trello.com";
        String trelloKey="8ff85dd7c9034c0059f7091752250727";
        String trelloToken="3fbf5c0818b000129053fb7b347f2e873ca3576d31f3fb9b80c898883a5989b5";
        DataSpotifyAPI spotifyReq = new DataSpotifyAPI();
        TrelloBoard tb=new TrelloBoard();
        TrelloList tl=new TrelloList();
        tb.setName("Bob Dylan Discography thread with threads");

        JSONArray spotifyData = spotifyReq.getSpotifyData(spotifyToken, spotifyUrl);//GET SPOTIFY DATA
        ArrayList<Album> albumList = rd.getData(discographyFile);//GET ALBUM DATA
        ArrayList<Album> inputTrelloData=idb.buildInputTrelloData(albumList,spotifyData);//BUILD INPUT DATA FOR TRELLO API
        tb.setLists(tl.sort(idb.buildTrelloLists(inputTrelloData)));//CREATE TRELLO BOARD LIST
        String trelloBoardId=tdp.sendTrelloBoard(trelloUrl,trelloKey,trelloToken,tb);//CREATE TRELLO BOARD
        System.out.println("Process completed, Trello Board "+trelloBoardId+" created.");

    }




}
