package trello.data;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataSpotify {
    public JSONObject getData(String path) {

        try {
            String text = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
            JSONObject obj=new JSONObject((text));
            return obj;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
