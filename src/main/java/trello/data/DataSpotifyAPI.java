package trello.data;

import org.json.JSONArray;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;
import trello.service.ReadData;


import static io.restassured.RestAssured.given;

public class DataSpotifyAPI {
	public JSONArray getSpotifyData(String token,String url)  {
		JSONArray items=new JSONArray();
		try {

			Response response =
					given()
							.headers(
									"Authorization",
									"Bearer " + token
							)
							.when()
							.get(url)
							.then()
							.contentType(ContentType.JSON)
							.extract()
							.response();
			JSONObject obj = new JSONObject(response.getBody().prettyPrint());
			items = obj.getJSONArray("items");
			return items;
		} catch (Exception e) {
			System.out.println("Can't access spotify api, getting data from classpath...");
			ReadData rd=new ReadData();
			String path = "src/main/java/trello/assets/spotify-data.json";
			return rd.readSpotifyDataFromFile(path).getJSONArray("items");
		}

	}
}