package trello.data;

import trello.domain.Album;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class DataAlbum {
    public ArrayList<Album> getData(File discographyFile) {
        ArrayList<Album> albumList=new ArrayList<Album>();
        try {

            Scanner myReader = new Scanner(discographyFile);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String year=data.substring(0,4);
                String name=data.substring(5,data.length());
                Album theAlbum=new Album (Integer.parseInt(year),name);
                albumList.add(theAlbum);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error!");
            e.printStackTrace();
        }

        return albumList;

    }
}
