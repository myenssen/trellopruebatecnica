package trello.data;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;
import static io.restassured.RestAssured.given;

public class DataTrelloAPI {
    public String createTrelloBoard(String url,String key,String token,String name)  {
        try{
            Response response =
                    (Response) given().contentType(ContentType.JSON)
                            .queryParam("key", key)
                            .queryParam("token", token)
                            .queryParam("defaultLists", false)
                            .header("accept", "application/json")
                            .when()
                            .post(url + "/1/boards/?name="+name)
                            .then()
                            .contentType(ContentType.JSON)
                            .extract();
            JSONObject obj = new JSONObject(response.getBody().prettyPrint());
            return obj.getString("id");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createTrelloList(String url,String key,String token,String boardId,String listName) {
        try {
            Response response =
                    (Response) given().contentType(ContentType.JSON)
                            .queryParam("key", key)
                            .queryParam("token", token)
                            .header("accept", "application/json")
                            .when()
                            .post(url + "/1/boards/"+boardId+"/lists?name="+listName)
                            .then()
                            .contentType(ContentType.JSON)
                            .extract();
            JSONObject obj = new JSONObject(response.getBody().prettyPrint());
            return obj.getString("id");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
        public String createTrelloCard (String url, String key, String token,String listId,String cardName){
            try {
                Response response =
                        (Response) given().contentType(ContentType.JSON)
                                .queryParam("key", key)
                                .queryParam("token", token)
                                .queryParam("name", cardName)
                                .header("accept", "application/json")
                                .when()
                                .post(url + "/1/cards?idList="+listId)
                                .then()
                                .contentType(ContentType.JSON)
                                .extract();
                JSONObject obj = new JSONObject(response.getBody().prettyPrint());
                return obj.getString("id");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

       /* public String sendTrelloBoard(String url, String key, String token,TrelloBoard tb){
            String boardId=createTrelloBoard(url,key,token,tb.getName());
            ArrayList<String> listIds=new ArrayList<>();
            for(int i=0;i<tb.getLists().size();i++){
                listIds.add(createTrelloList(url,key,token,boardId,tb.getLists().get(i).getName()));
            }
            for(int i=0;i<listIds.size();i++){
                for(int j=0;j<tb.getLists().get(j).getCardList().size();j++){
                    createTrelloCard(url,key,token,listIds.get(i),tb.getLists().get(i).getCardList().get(j).getName());
                }

            }
            return boardId;
        }*/

}
