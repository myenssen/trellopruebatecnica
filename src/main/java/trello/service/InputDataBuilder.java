package trello.service;

import org.json.JSONArray;
import trello.domain.Album;
import trello.domain.TrelloCard;
import trello.domain.TrelloList;
import java.util.ArrayList;

public class InputDataBuilder {
    public ArrayList<Album> buildInputTrelloData(ArrayList<Album> list, JSONArray spotifyData) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < spotifyData.length(); j++) {
                String name = spotifyData.getJSONObject(j).getString("name");
                JSONArray images = spotifyData.getJSONObject(j).getJSONArray("images");
                if (list.get(i).getName().toLowerCase().compareTo(name.toLowerCase()) == 0) {
                    String img = images.getJSONObject(1).getString("url");
                    list.get(i).setImage(img);
                    break;
                }
            }
        }
       /* for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getYear() + " " + list.get(i).getName() + " " + list.get(i).getImage());
        }*/
        return list;
    }

    public ArrayList<TrelloList> buildTrelloLists(ArrayList<Album> albumList){
        ArrayList<TrelloList> list=new ArrayList<TrelloList>();
        for(int i=0;i<albumList.size();i++){
            boolean exists=false;
            if(list.size()==0){
                TrelloList tl= new TrelloList();
                tl.setName(Integer.toString(albumList.get(i).getYear()));
                TrelloCard card=new TrelloCard();
                card.setName(albumList.get(i).getName());
                tl.getCardList().add(card);
                list.add(tl);
            }else {
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getName().compareTo(Integer.toString(albumList.get(i).getYear())) == 0) {
                        exists=true;
                        TrelloCard card=new TrelloCard();
                        card.setName(albumList.get(i).getName());
                        list.get(j).getCardList().add(card);
                    }
                }
                if(!exists){
                    TrelloList tl = new TrelloList();
                    tl.setName(Integer.toString(albumList.get(i).getYear()));
                    TrelloCard card=new TrelloCard();
                    card.setName(albumList.get(i).getName());
                    tl.getCardList().add(card);
                    list.add(tl);
                }
            }

        }
        return list;
    }

}
