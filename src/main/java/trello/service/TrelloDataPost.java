package trello.service;

import trello.data.DataTrelloAPI;
import trello.domain.AddTask;
import trello.domain.TrelloBoard;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class TrelloDataPost {
    public String createTrelloBoard(String url,String key,String token,String name){
        DataTrelloAPI dta=new DataTrelloAPI();
        String id=dta.createTrelloBoard(url,key,token,name);
        return id;
    }
    public String createTrelloList(String url,String key,String token,String boardId,String name){
        DataTrelloAPI dta=new DataTrelloAPI();
        String id=dta.createTrelloList(url,key,token,boardId,name);
        return id;
    }
    public String createTrelloCard(String url,String key,String token,String listId,String name){
        DataTrelloAPI dta=new DataTrelloAPI();
        String id=dta.createTrelloCard(url,key,token,listId,name);
        return id;
    }
    public String sendTrelloBoard(String url, String key, String token, TrelloBoard tb) throws ExecutionException, InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(10);
        String boardId=createTrelloBoard(url,key,token,tb.getName());
        ArrayList<String> listIds=new ArrayList<>();
        //TODO:MY IDEA WAS SEND REQUESTS THROUGH THREADS BUT I COULD NOT GET THE ALBUMS ORDERED - IMPLEMENT THREADS AND GET A WAY TO ORDER THE ALBUMS IN TRELLO
        List<Future<String>> futures = new ArrayList<>();
        for(int i=0;i<tb.getLists().size();i++){
            String name=tb.getLists().get(i).getName();
            futures.add(es.submit(new AddTask(url,key,token,boardId,name,tb)));
        }
        return boardId;
//        try{
//            for(int i=0;i<tb.getLists().size();i++){
//                String name=tb.getLists().get(i).getName();
//                listIds.add(createTrelloList(url,key,token,boardId,name));
//            }
//            //TODO: I ALREADY HAVE THE SPOTIFY COVER INFO IN MY PROGRAM, BUT TRELLO ACCEPTS Unsplash URL ONLY - POST ATTACHMENTS AND THEN TAKE DE COVER FROM THERE
//            for(int i=0;i<listIds.size();i++){
//                String idList=listIds.get(i);
//                for(int j=0;j<tb.getLists().get(i).getCardList().size();j++){
//                    String cardName=tb.getLists().get(i).getCardList().get(j).getName();
//                    createTrelloCard(url,key,token,idList,cardName);
//                }
//
//            }
//            return boardId;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
    }



}
