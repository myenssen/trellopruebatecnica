package trello.service;
import org.json.JSONArray;
import org.json.JSONObject;
import trello.data.DataAlbum;
import trello.data.DataSpotify;
import trello.domain.Album;

import java.io.File;
import java.util.ArrayList;


public class ReadData {
    public ArrayList<Album> getData(File discographyFile) {
        DataAlbum dt=new DataAlbum();
        return dt.getData(discographyFile);
    }
    public JSONObject readSpotifyDataFromFile(String url){
        DataSpotify ds=new DataSpotify();
        return ds.getData(url);
    }
}
