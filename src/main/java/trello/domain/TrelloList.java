package trello.domain;

import java.util.ArrayList;

public class TrelloList {
    private String name;
    private ArrayList<TrelloCard> cardList =new ArrayList<TrelloCard>();

    public TrelloList() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TrelloCard> getCardList() {
        return cardList;
    }

    public void setCardList(ArrayList<TrelloCard> cardList) {
        this.cardList = cardList;
    }

    public TrelloList(String name, ArrayList<TrelloCard> cardList) {
        this.name = name;
        this.cardList = cardList;
    }

    public static ArrayList<TrelloList> sort(ArrayList<TrelloList> list)
    {

        list.sort((o1, o2)
                -> o2.getName().compareTo(
                o1.getName()));
        return list;

    }
}
