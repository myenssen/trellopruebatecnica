package trello.domain;

import java.util.ArrayList;

public class TrelloBoard {
    private String name;
    private ArrayList<TrelloList> lists =new ArrayList<TrelloList>();

    public TrelloBoard() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TrelloList> getLists() {
        return lists;
    }

    public void setLists(ArrayList<TrelloList> lists) {
        this.lists = lists;
    }

    public TrelloBoard(String name, ArrayList<TrelloList> lists) {
        this.name = name;
        this.lists = lists;
    }
}
