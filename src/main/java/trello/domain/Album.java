package trello.domain;

public class Album {
    private String name;
    private int year;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String images) {
        this.image = images;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public Album(int year,String name) {
        super();
        this.year = year;
        this.name = name;
    }
    public Album() {
        super();
    }



}
