package trello.domain;

import trello.service.TrelloDataPost;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class AddTask implements Callable<String> {
    private String url;
    private String key;
    private String token;
    private String boardId;
    private String listName;
    private TrelloBoard tb;

    public AddTask(String url, String key, String token, String boardId, String listName,TrelloBoard tb) {
        this.url = url;
        this.key = key;
        this.token = token;
        this.boardId = boardId;
        this.listName = listName;
        this.tb=tb;
    }

    @Override
    public String call() throws Exception {
        TrelloDataPost tdp=new TrelloDataPost();
        String idList= tdp.createTrelloList(url,key,token,boardId,listName);
        for(int i=0;i<this.tb.getLists().size();i++){
            if(this.tb.getLists().get(i).getName().compareTo(listName)==0){
                for(int j=0;j<this.tb.getLists().get(i).getCardList().size();j++){
                    tdp.createTrelloCard(url,key,token,idList,this.tb.getLists().get(i).getCardList().get(j).getName());
                }

            }
        }
        return idList;
    }

}
