package trello.domain;

public class TrelloCard {
    private String name;

    public TrelloCard() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrelloCard(String name) {
        this.name = name;
    }
}
